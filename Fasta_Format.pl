### Program utilized to read in and extract FASTA files ###

## Given a filename, pull data

sub acq_file_data {
  my($filename) = @_;

  use strict;
  use warnings;

  # init variables

  my @filedata = ();

  unless (open(acq_file_data, $filename)) {
    # body...
    print STDERR " Unable to open file \"$filename\"\n\n";
    exit;
  }

  @filedata = <acq_file_data>;
  close acq_file_data;
  return @filedata;

}

## Once data is found, extract FASTA seq from Array

sub ext_seq_fasta_data {

  my(@fasta_data) = @_;

  use strict;
  use warnings;

  # init variables

  my $sequence = '';

  foreach my $line (@fasta_data) {
    # body...
    # Remove blank line
    if ($line =~ /^\s*$/){
      next;

    # Remove comment line
  } elsif($line =~ /^\s*#/) {
      next;

    # Remove fasta header line
  } elsif($line =~ /^>/) {
    next;

    # Retain line, append to seq string
  } else {
    $sequence .= $line;
  }

  }
  # Cleans up whitespace from $sequence string
  $sequence =~ s/\s//g;
  return $sequence;
}

## Subroutine used to format and print seq data

sub print_seq {

my($sequence, $length) = @_;

use strict;
use warnings;

# Print seq out in lines of $length
for (my $posit = 0; $posit < length($sequence); $posit += $length){
  print substr($sequence, $posit, $length), "\n";
}

}
